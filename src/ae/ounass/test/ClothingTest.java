package ae.ounass.test;

import static ae.ounass.test.Constants.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ae.ounass.pages.ClothingPage;

public class ClothingTest {

	public RemoteWebDriver driver;
	public ClothingPage clothingPage;

	@Parameters({ "browserType", "hubURL", "environment" })
	@BeforeClass
	public void setup(String browserType, String hubURL, String environment) throws MalformedURLException {

		Platform platfom = null;
		if (environment.equals("Windows"))
			platfom = org.openqa.selenium.Platform.WINDOWS;
		else
			platfom = org.openqa.selenium.Platform.LINUX;
		DesiredCapabilities dr = null;
		if (browserType.equals("chrome")) {
			System.setProperty("Webdriver.chrome.driver", DRIVER_PATH);
			dr = DesiredCapabilities.chrome();
			dr.setBrowserName("chrome");
			dr.setPlatform(platfom);
		} else {
			System.setProperty("Webdriver.firefox.marionette", "false");
			System.setProperty("Webdriver.firefox.driver", FIREFOX_DRIVER_PATH);
			dr = DesiredCapabilities.firefox();
			dr.setBrowserName("firefox");
			dr.setPlatform(platfom);
		}
		driver = new RemoteWebDriver(new URL(hubURL), dr);
		clothingPage = PageFactory.initElements(driver, ClothingPage.class);
		driver.get(WEBSITE);
		clothingPage.waitForPageToLoad();
		clothingPage.popupClose();
		WebDriverWait check = new WebDriverWait(driver, 100, 1000);
		check.until(ExpectedConditions.presenceOfElementLocated(By.className(OUNASS_LOGO_ID_LOCATOR)));
		driver.manage().window().maximize();
	}

	@DataProvider(name = "ScrollPageDataProvider")
	public Object[][] scrollItems() {
		return new Object[][] { { 48 }, { 72 }, { 72 } };
	}

	@Test(description = "Validate number of elements shown on each scroll", dataProvider = "ScrollPageDataProvider")
	public void validateElementCountOnPageScroll(int itemCount) throws InterruptedException, IOException {
		clothingPage.scrollPage();
		Thread.sleep(2000);
		Assert.assertEquals(clothingPage.getTotalItems(), itemCount);
	}

	@Test(description = "Validate view more button click loads next set of products", dependsOnMethods = {
			"validateElementCountOnPageScroll" })
	public void ValidateViewMoreButtonClick() throws InterruptedException, IOException {
		clothingPage.loadBtnClick();
		Thread.sleep(2000);
		Assert.assertEquals(clothingPage.getTotalItems(), 96);
	}

	@DataProvider(name = "sortByOptionProvider")
	public Object[][] sortByOptionDataProvider() {
		return new Object[][] { { "Sort by Newest", 24 }, { "Sort by Low Price", 24 }, { "Sort by High Price", 24 } };
	}

	@Test(description = "Validate results are reset to 24 on selecting sort by options", dataProvider = "sortByOptionProvider", dependsOnMethods = {
			"ValidateViewMoreButtonClick" })
	public void validateResetResultsOnSortBy(String option, int value) throws InterruptedException {
		clothingPage.selectSortByOption(option);
		Assert.assertEquals(clothingPage.getTotalItems(), value);
	}

	@DataProvider(name = "SelectedItemDataProvider")
	public Object[][] selectedItems() {
		return new Object[][] { { "more designers ... >", new ArrayList<String>() {
			{
				add("Christian Siriano");
				add("2 results found");
				add("2");
			}
		} } };
	}

	@Test(description = "Validate only 2 items are shown when a filter is applied on brand Christian Siriano", dataProvider = "SelectedItemDataProvider", dependsOnMethods = {
			"validateResetResultsOnSortBy" })
	public void validateFilterResults(String categoryTypeBtn, ArrayList<String> selectedItem)
			throws InterruptedException, IOException {
		if (clothingPage.moreItemBtn().contains(categoryTypeBtn)) {
			clothingPage.clickMoreItemBtn(categoryTypeBtn);
		}
		clothingPage.selectFilters(selectedItem.get(0));
		Thread.sleep(2000);
		Assert.assertTrue(clothingPage.getSelectedFilterItem().contains(selectedItem.get(0)));
		Assert.assertEquals(clothingPage.getTotalItemsDisplayed(), selectedItem.get(1));
		Assert.assertEquals(clothingPage.getItemTitle(), selectedItem.get(0));
		Assert.assertEquals(clothingPage.getTotalItems(), Integer.parseInt(selectedItem.get(2)));
		ArrayList<String> brandList = (ArrayList<String>) clothingPage.getDisplayedItemCategory();
		for (String name : brandList) {
			Assert.assertEquals(name, selectedItem.get(0).toUpperCase());
		}
	}
	public RemoteWebDriver getDriver() {
		return driver;
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
