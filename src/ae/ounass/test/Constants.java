package ae.ounass.test;

public class Constants {
	

	public static final String SCREENSHOT_FOLDER = "output/screenshots";
	public static final String DRIVER_PATH = "src/libs/chromedriver.exe" ;///\\chromedriver.exe"; //"C:\\Users\\vanitvan\\Downloads\\chromedriver_win32\\chromedriver.exe";
	public static final String FIREFOX_DRIVER_PATH = "src/libs/geckodriver.exe" ;//"C:\\ProgramData\\Oracle\\Java\\javapath\\geckodriver.exe";
	public static final String WEBSITE = "https://www.ounass.ae/clothing/";
	public static final String OUNASS_LOGO_ID_LOCATOR = "logo";
}
