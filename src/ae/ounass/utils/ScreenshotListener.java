package ae.ounass.utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;
import ae.ounass.test.ClothingTest;
public class ScreenshotListener implements ITestListener {
	   @Override
	    public void onTestFailure(ITestResult result) {
	        Calendar calendar = Calendar.getInstance();
	        SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
	        String methodName = result.getName();
	        if(!result.isSuccess()){
	        	
	        	Object currentClass = result.getInstance();
	            WebDriver webDriver = ((ClothingTest) currentClass).getDriver();
	            File scrFile = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
	        
	            try {
	               	String screenShotFolder = String.format("%s/%s", System.getProperty("user.dir"),  ae.ounass.test.Constants.SCREENSHOT_FOLDER);
	                File destFile = new File((String) screenShotFolder+"/failure_screenshots/"+methodName+"_"+formater.format(calendar.getTime())+".png");
	                FileUtils.copyFile(scrFile, destFile);
	                //Add image path to report.
	                Reporter.log("<a href='"+ destFile.getAbsolutePath() + "'> <img src='"+ destFile.getAbsolutePath() + "' height='100' width='100'/> </a>");
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	   
	   @Override
	    public void onTestStart(ITestResult iTestResult) {}

	    @Override
	    public void onTestSuccess(ITestResult iTestResult) {}

		@Override
	    public void onTestSkipped(ITestResult iTestResult) {}

	    @Override
	    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {}

	    @Override
	    public void onStart(ITestContext iTestContext) {}

	    @Override
	    public void onFinish(ITestContext iTestContext) {}

}
