package ae.ounass.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



public class ClothingPage {
	
WebDriver driver;
	
	public ClothingPage(WebDriver driver)
	{	
		this.driver= driver;
	}
		
	@FindBy(xpath = "//div[@class=\"product-item-wrapper col-md-4 col-sm-4 col-6\"]")
	List<WebElement> allItems;
	@FindBy(xpath ="//div[@class=\"col-12 col-md-push-3 col-md-6 button-wrapper\"]")
	WebElement loadMoreBtn;
	@FindBy(className ="toggle-more")
	List<WebElement> selectMoreBtn;
	@FindBy(xpath ="//a[@class=\"filter-value-item needsclick\"]")
	List<WebElement> selectFilters;
	@FindBy(xpath ="//a[@class=\"filter-value-item selected needsclick\"]")
	List<WebElement> selectedItems;
	@FindBy(className = "total-items")
	WebElement totalItemsDisplayed;
	@FindBy(xpath = "//h1[@class=\"search-results-header f-hbox f-main-center overflow-visible\"]/a")
	WebElement itemCategoryTitle;
	@FindBy(xpath ="//div[@class=\"product-title\"]/strong/div")
	List<WebElement> itemCategory;
	@FindBy(xpath = "//*[@id=\"onesignal-popover-cancel-button\"]")
	WebElement newsLetter;
	@FindBy(xpath = "//*[@id=\"newsletter-modal\"]/div/div/div/div[1]/button")
	WebElement newsLetterForm;
	@FindBy(className = "sorting-field")
	WebElement sortBySelectionBtn;
	
	public void scrollPage() throws IOException
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;		
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
       
        
	}
	
	public void waitForPageToLoad()
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(newsLetter));
			wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(newsLetterForm));
		}
		catch(Exception e)
		{
			//Do nothing
		}
	}
	
	public void popupClose()
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(newsLetter));
			newsLetter.click();
	      
		}
		catch(Exception e)
		{
			//Do nothing
		}
		try
		{
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOf(newsLetterForm));
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].click()", newsLetterForm); 
	      ;
		}
		catch(Exception e)
		{
			//Do nothing
		}
	}
	
	public int getTotalItems()
	{
		return allItems.size();
		
	}
	
	public void loadBtnClick() throws IOException
	{
		loadMoreBtn.click();
       

	}
	
	public List<String> moreItemBtn()
	{
		List<String> moreItemBtn = new ArrayList<>();
		for(WebElement element : selectMoreBtn)
		{
			moreItemBtn.add(element.getText());
		}
		
		return moreItemBtn;
	}
	
	public void clickMoreItemBtn(String clickBtn) throws IOException
	{
		for(WebElement element : selectMoreBtn)
		{
			if(element.getText().equals(clickBtn))
			{
				JavascriptExecutor js = (JavascriptExecutor) driver;		
		        js.executeScript("arguments[0].click();", element);
		     
				break;
			}
		}
		
	}
	
	public void selectFilters(String filterValue) throws IOException
	{
		for(WebElement element : selectFilters)
		{
			if(element.getText().equals(filterValue))
			{
				JavascriptExecutor js = (JavascriptExecutor) driver;		
		        js.executeScript("arguments[0].click();", element);
		  
				break;
			}
		}
	}
	
	public List<String> getSelectedFilterItem()
	{
		List<String> selectedItem = new ArrayList<>();
		for(WebElement element : selectedItems)
		{
			selectedItem.add(element.getText());
		}
		
		return selectedItem;
	}
	
	public String getTotalItemsDisplayed()
	{
		return totalItemsDisplayed.getText();
	}
	
	public String getItemTitle()
	{
		return itemCategoryTitle.getText();
	}

	public List<String> getDisplayedItemCategory()
	{
		List<String> displayedItem = new ArrayList<>();
		for(WebElement element : itemCategory)
		{
			displayedItem.add(element.getText());
		}
		
		return displayedItem;
	}
	
	public void selectSortByOption(String option) throws InterruptedException
	{
		Select sortByOptions = new Select(sortBySelectionBtn);
		sortByOptions.selectByVisibleText(option);
		Thread.sleep(5000);
	}
}
